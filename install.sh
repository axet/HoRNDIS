#!/bin/bash

set -e 

# fix for "tar: unable to record current working directory: No such file or directory"
untar() {
  cd "$@" && sudo tar xpo
}

manual() {
  DIRS="/System/Library/Extensions/ /Library/Extensions/"

  for DIR in $DIRS; do
    sudo rm -rf "$DIR/HoRNDIS.kext" || exit 1
  done

  DIR="/Library/Extensions/"

  tar c -C "./build/Release/" HoRNDIS.kext/ | untar "$DIR" || exit 1
  sudo kextload "$DIR/HoRNDIS.kext" || exit 1
}

pkg() {
  TMP=$(mktemp -d)
  cp ./build/HoRNDIS.pkg "$TMP" || exit 1
  for PKG in "$TMP"/*; do
    sudo installer -package "$PKG" -target / || exit 1
  done
  rm -rf "$TMP" || exit 1
  DIR="/System/Library/Extensions/"
  sudo kextload "$DIR/HoRNDIS.kext" || exit 1
}

# error: invalid deployment target for -stdlib=libc++ (requires OS X 10.7 or later)

xcodebuild -project HoRNDIS.xcodeproj clean
xcodebuild -project HoRNDIS.xcodeproj build CODE_SIGN_IDENTITY="$CODE_SIGN_IDENTITY" DEVELOPMENT_TEAM="$DEVELOPMENT_TEAM"
xcodebuild -configuration Release-unsigned -project HoRNDIS.xcodeproj CODE_SIGN_IDENTITY="$CODE_SIGN_IDENTITY" DEVELOPMENT_TEAM="$DEVELOPMENT_TEAM"

make build/HoRNDIS.pkg

# https://developer.apple.com/library/mac/documentation/Security/Conceptual/System_Integrity_Protection_Guide/KernelExtensions/KernelExtensions.html
csrutil status

pkg

kextstat | grep -o com.joshuawise.kexts.HoRNDIS && echo "Success."
